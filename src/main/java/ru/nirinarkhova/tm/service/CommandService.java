package ru.nirinarkhova.tm.service;

import ru.nirinarkhova.tm.api.ICommandRepository;
import ru.nirinarkhova.tm.api.ICommandService;
import ru.nirinarkhova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
