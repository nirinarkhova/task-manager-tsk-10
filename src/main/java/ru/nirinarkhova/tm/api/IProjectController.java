package ru.nirinarkhova.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

}
